package com.example.alexdonoso.primeraapp4c;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Fragmentos extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    Button botonFragUno, botonFragDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);

        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button)findViewById(R.id.btnFrgDos);

        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnFrgUno:
                FragUno frgmentoUno = new FragUno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.btnFrgDos:
                FragDos frgmentoDos = new FragDos();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoDos);
                transaction.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

